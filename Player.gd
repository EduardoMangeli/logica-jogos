extends Area2D # o personagem é do tipo Area2D

signal hit # definindo um sinal especial

# export permite fazer o ajuste no Inspector
export var speed = 400 # Velocidade do personagem (pixels/sec).
var screen_size # Tamanho da Janela.


# essa função é executada quando o nó entra na cena
func _ready():
	screen_size = get_viewport_rect().size
	# hide()

# essa função é executada a cada frame
func _process(delta): # delta é o tempo em segundos desde a última chamada da função
	pass


func start(pos):
	position = pos # posiciona o personagem
	show()
	$CollisionShape2D.disabled = false

# esse metodo vai emitir nosso sinal
func _on_Player_body_entered(_body):
	pass
	# hide() # personagem some quando colide
	# emit_signal("hit") # emite o sinal
	# informa que quer desabilitar o colisão e deixa o Godot fazer quando puder.
	# $CollisionShape2D.set_deferred("disabled", true)
