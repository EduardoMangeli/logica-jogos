# Material do Workshop Lógica de Programação no Desenvolvimento de Jogos - Um Estudo de Caso com a Game Engine Godot

Esse código e parte do jogo Dodge de Creeps do tutorial do Engine Godot
["Your first 2D game"](https://docs.godotengine.org/en/latest/getting_started/first_2d_game/index.html)

Linguagem: GDScript

Renderizador: GLES 3 

Testado na versão: [LTS 3.5.2](https://godotengine.org/download/3.x/windows/)


## Screenshots

![GIF from the documentation](https://docs.godotengine.org/en/latest/_images/dodge_preview.gif)

![Screenshot](screenshots/dodge.png)

## Copying

`art/House In a Forest Loop.ogg` Copyright &copy; 2012 [HorrorPen](https://opengameart.org/users/horrorpen), [CC-BY 3.0: Attribution](http://creativecommons.org/licenses/by/3.0/). Source: https://opengameart.org/content/loop-house-in-a-forest

IImagens de "Abstract Platformer". Criado em 2016 por kenney.nl, [CC0 1.0 Universal](http://creativecommons.org/publicdomain/zero/1.0/). Source: https://www.kenney.nl/assets/abstract-platformer

A font é "Xolonium". Copyright &copy; 2011-2016 Severin Meyer <sev.ch@web.de>, with Reserved Font Name Xolonium, SIL open font license version 1.1. Details are in `fonts/LICENSE.txt`.
